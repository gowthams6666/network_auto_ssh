from pexpect import pxssh
import pexpect
import getpass


def login(hostname='',username='',password='',login_timeout=6,etimeout=5):
    # Login to NPCI device , "enable" password check disabled because of aaa conf in NPCI
    try:
        s = pxssh.pxssh(options={
                        "StrictHostKeyChecking": "no",
                        "UserKnownHostsFile": "/dev/null"},timeout=login_timeout)
        s.login(hostname, username, password,auto_prompt_reset=False,login_timeout=login_timeout)
        # Send enter to get router prompt to check login success
        s.sendline('')
        # expecting cisco , juniper , fortigate prompt 
        s.expect(["#",">","\$",pexpect.TIMEOUT],timeout=etimeout)
        login_chk = s.before
        if len(login_chk) > 0:
            host_name = str(login_chk.strip())
            aftr = s.after
            if type(aftr) == str:
                host_name = host_name+aftr.strip()
            print "Login Success :"+host_name
            return s,host_name
        else:
            print "Not able to predict login-success"
        return "UNKNOWN"
    except pxssh.ExceptionPxssh as e:
        err = str(e)
        if err.find("password refused") != -1:
            print "Login Failed"
            return "LOGINFAIL"
        else:
            print("Error>"+err)
        return "TIMEOUT"
    except Exception as e:
        #print("Unknown Error"+str(e))
        return "TIMEOUT"


if raw_input("Enter Yes to Run").find("yes") < 1:
	exit()
op = open("output.txt","ab+")
for i in open("ip.txt"):
    try:
        print "Now Trying: "+i
        s = login(i.strip(),'gowtham','G0wtham@2',15,15)
        if s != str or None:
                for cmd in open("input.txt"):
			s[0].sendline(cmd)
		s[0].expect([s[1],pxssh.TIMEOUT],timeout=20)
		io = s[0].before
		print io
		print s[0].after
                s[0].logout()
         else:
                print "Failed :"+str(s)+str(i)
                op.write("Failed :"+str(s)+str(i))
    except :
        op.write("NOT ABLE TO LOGIN:"+str(i))
        print "NOT ABLE TO LOGIN:"+str(i)
op.close()