import telnetlib
import time
import datetime

class telnet_session():
    def __init__(self,host,time_out=10,input_f='input.txt',output_f='output.txt'):
        self.host = host
        self.time_out = time_out
        self.png_result = []
        self.input_f = input_f
        self.output_f = output_f
    def read_test_file(self,input_f):
        try:
            f = open(input_f).read()
            f2 = f.split('\n')
            return f2
        except Exception as e:
            print e
    def run(self):
        try:
            print ('Trying : '+self.host)
            self.tn = telnetlib.Telnet(self.host,23)
            op = open(self.output_f,'a+')
            op.write('\n'+'='*40+'\n')
            op.write(self.host+' : '+str(datetime.datetime.now())+'\n')
            for x in self.read_test_file(self.input_f):
                if len(x) > 0:
                    if x[0] == '>':
                        self.tn.write(x[1:]+'\n')
                    elif x[0:2] == '<<':
                        out = self.tn.expect([x[2:]],self.time_out)
                        print out[2]
                        op.write(str(out[2]))
                    elif x[0] == '<':
                        out = self.tn.read_until(x[1:],self.time_out)
                        print out
                        op.write(out)
                    else:
                        op.write('UNKNOWN SYNTEX: '+x)
                        print 'UNKNOWN SYNTEX: '+x
            op.close()
            self.tn.close()
            print ('Done : '+self.host)
        except Exception as e:
            print 'Telnet Error: '+str(e)

#t = telnet_session('192.168.176.188',5)
#t.run()
