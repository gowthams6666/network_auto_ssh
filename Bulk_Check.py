from pexpect import pxssh
import pexpect
import getpass


def login(hostname='',username='',password='',login_timeout=6,etimeout=5):
    # Login to NPCI device , "enable" password check disabled because of aaa conf in NPCI
    try:
        s = pxssh.pxssh(options={
                        "StrictHostKeyChecking": "no",
                        "UserKnownHostsFile": "/dev/null"},timeout=login_timeout)
        s.login(hostname, username, password,auto_prompt_reset=False,login_timeout=login_timeout)
        # Send enter to get router prompt to check login success
        s.sendline('')
        # expecting cisco , juniper , fortigate prompt 
        s.expect(["#",">","\$",pexpect.TIMEOUT],timeout=etimeout)
        login_chk = s.before
        if len(login_chk) > 0:
            host_name = str(login_chk.strip())
            aftr = s.after
            if type(aftr) == str:
                host_name = host_name+aftr.strip()
            print "Login Success :"+host_name
            return s,host_name
        else:
            print "Not able to predict login-success"
        return "UNKNOWN"
    except pxssh.ExceptionPxssh as e:
        err = str(e)
        if err.find("password refused") != -1:
            print "Login Failed"
            return "LOGINFAIL"
        else:
            print("Error>"+err)
        return "TIMEOUT"
    except Exception as e:
        #print("Unknown Error"+str(e))
        return "TIMEOUT"

ip = """192.168.251.180
192.168.251.188
192.168.251.244
192.168.251.220
192.168.251.228
192.168.248.156
192.168.251.252
192.168.252.4
192.168.251.172
192.168.252.28
192.168.248.228
192.168.252.12
192.168.252.36
192.168.252.44
192.168.252.52
192.168.252.60
192.168.252.68
192.168.255.28
192.168.255.29
192.168.252.92
192.168.252.100
192.168.251.108
192.168.252.108
192.168.252.132
192.168.252.20
192.168.254.125
192.168.252.148
192.168.252.172
192.168.252.164
192.168.252.188
192.168.252.180
192.168.252.196
192.168.251.148
192.168.252.204
192.168.252.228
192.168.252.212
"""

ip = ip.split("\n")
for i in ip:
    try:
        
        print "Now Trying: "+i
        s = login(i,'gowtham','G0wtham@2',15,15)
        if s[0] != None:
            if s != str or None:
                s[0].sendline("show fla")
                s[0].expect([s[1],pxssh.TIMEOUT],timeout=20)
                io = s[0].before
                if str(io).find("c3560e-universalk9-mz.152-4.E1.bin") != -1:
                    print io
                else:
                    print "NOT UPLODED"
                    
            s[0].logout()
        else:
            print "Not able to reach:"+str(i)
    except :
        print "NOT ABLE TO LOGIN:"+str(i)
